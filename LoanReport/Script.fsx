#load "LoanReport.fs"
open LoanReport

// Define your library scripting code here
type Client =
  { Name : string
    Income : int
    YearsInJob : int
    UsesCreditCard : bool
    CriminalRecord : bool }
let john = 
    { Name = "John Doe"; Income = 40000; YearsInJob = 1
      UsesCreditCard = true; CriminalRecord = false }
let tests =
    [ (fun cl -> cl.CriminalRecord);
      (fun cl -> cl.Income < 30000);
      (fun cl -> not cl.UsesCreditCard);
      (fun cl -> cl.YearsInJob < 2) ]
    
let testClient(client) =
    let issues = tests |> List.filter (fun f -> f (client))
    let suitable = issues.Length <= 1
    printfn "Client: %s\nOffer a loan: %s (issues = %d)" client.Name
            (if (suitable) then "YES" else "NO") issues.Length

let checkCriminal(client) = client.CriminalRecord

// When writing type declarations in F#, we can only refer to the types declared earlier in
// the file (or in a file specified earlier in the compilation order or located higher in the
// Visual Studio solution). Obviously that’s going to cause problems in this situation,
// where we want to define two types that reference each other. To get around this, F#
// includes the and keyword. The type declaration in the listing starts as usual with the
// type keyword B, but it continues with and D, which means that the two types are
// declared simultaneously and can see each other.
type QueryInfo =
  { Title : string
    Check : Client -> bool 
    Positive : Decision
    Negative : Decision }
and Decision = 
  | Result of string
  | Query of QueryInfo

let usesCredit =
    Query({ Title = "Uses credit card"
            Check = (fun cl -> cl.UsesCreditCard)
            Positive = Result("YES"); Negative = Result("NO") })
let moreThan40 =
    Query({ Title = "Has criminal record"
            Check = (fun cl -> cl.CriminalRecord)
            Positive = Result("NO"); Negative = Result("YES") })
let lessThan40 =
    Query({ Title = "Years in job"
            Check = (fun cl -> cl.YearsInJob > 1)
            Positive = Result("YES"); Negative = usesCredit })
let tree =
    Query({ Title = "More than $40k"
            Check = (fun cl -> cl.Income > 40000)
            Positive = moreThan40; Negative = lessThan40 })
let rec testClientTree(client, tree) =
    match tree with
    | Result(message) ->
        printfn " OFFER A LOAN: %s" message
    | Query(qinfo) ->
        let result, case =
            if (qinfo.Check(client)) then "yes", qinfo.Positive
            else "no", qinfo.Negative
        printfn " - %s? %s" qinfo.Title result
        testClientTree(client, case)

// Using records of functions
type ClientTest =
    { Check : Client -> bool
      Report : Client -> unit }
let testCriminal(client) = client.CriminalRecord
let reportCriminal(client) =
    printfn "'%s' has a criminal record!" client.Name
let tests2 =
    [ { Check = testCriminal; Report = reportCriminal } ]